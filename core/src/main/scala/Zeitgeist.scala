package iris.zeit.core

import iris.axis.ax._
import iris.axis.axr._
import iris.axis.axr.audio._
import com.badlogic.gdx
import com.badlogic.gdx.Gdx

import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer


class Zeitgeist(title: String = "Zeitgeist", screenWidth: Int = 850, screenHeight: Int = 458, width: Int = 850, height:Int = 458) 
extends Game(title, 30, screenWidth, screenHeight, width, height) {
	Game.debug = true
	
	override def start(){
		Sprite.loadSpritesAndImages(('boxy,"spritesheet.png",5,6))
		var scene = new Scene()
		
		new Controller().addControl(Symbol("19") , 'both , ()=>{
			scene.camera.translate(0f,1f,0f)
			Unit
		}).addControl(Symbol("20") , 'both , ()=>{
			scene.camera.translate(0f,-1f,0f)
			Unit
		}).addControl(Symbol("21") , 'both , ()=>{
			scene.camera.translate(-1f,0f,0f)
			Unit
		}).addControl(Symbol("22") , 'both , ()=>{
			scene.camera.translate(1f,0f,0f)
			Unit
		})
		
		//new Actor(850/2-100, 600/2+100).setSprite("boxy").setPhysics(new shapes.Circle(18), 'dynamic, 1, 0.2, 0.5)
		
		
		//new Actor(850/2).setPhysics(new shapes.Box(200,20), 'static)
		
		new Music("rain", "rain.ogg") play;
		
	}
	
	override def draw(){
	}
	
	override def dispose(){
		Music.disposeAll()
	}
}
