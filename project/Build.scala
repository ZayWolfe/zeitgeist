import sbt._
import Keys._

object Zeitgeist extends Build {
	lazy val axis = Project("axis", file("axis"))
	
	lazy val resources = Project("resources", file("resources"))
	
	lazy val core = Project("core", file("core")) dependsOn(axis)
	
	//lazy val editor = Project("desktop", file("editor")) dependsOn(axis, resources)
	
	lazy val aaaDesktop = Project("desktop", file("desktop")) dependsOn(core, resources)
}
