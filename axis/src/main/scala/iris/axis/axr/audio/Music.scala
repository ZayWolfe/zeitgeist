package iris.axis.axr.sound

import com.badlogic.gdx.Gdx

class Music(val name: String, val path: String, doLoop: Boolean = true) {
	var track = Gdx.audio.newMusic(Gdx.files.internal(path))
	track.setLooping(doLoop)
	
	def play(){
		track.play()
		this
	}
	def stop(){
		track.stop()
		this
	}
	
	def dispose(){
		track.dispose()
	}
	
	Music._cacheMusic(this)
}

object Music{
	private var cache: Array[Music] = new Array[Music](0)
	
	def _cacheMusic(track: Music): Unit = {
		cache = cache :+ track
	}
	
	def getMusic(name: String): Music = {
		var found_music: Music = null.asInstanceOf[Music]
		
		for(track <- cache){
			if(track.name == name){
				found_music = track
			}
		}
		found_music
	}
	
	def loadMusic(music: Tuple3[String, String, Boolean]*){
		for(track <- music){
			new Music(track._1, track._2, track._3)
		}
	}
	
	def disposeAll(){
		for(track <- cache){
			track.dispose()
		}
	}
	
}
