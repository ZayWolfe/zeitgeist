package iris.axis.axr

object Axr {
	def disposeAll(){
		audio.Music.disposeAll()
		Image.disposeAll()
	}
}
