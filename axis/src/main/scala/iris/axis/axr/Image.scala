package iris.axis.axr

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch

/** Image
 * 
 * Just a wrapper for libgdx's Texture object
 */
class Image(val name: Symbol, path: String) {
	private val img = new Texture(Gdx.files.internal(path))
	
	val width = img.getWidth()
	val height = img.getHeight()
	
	def _get_image_data(): Texture = {img}
	
	def _draw(btch: SpriteBatch, x: Double = 0.0, y: Double = 0.0){
		btch.draw(img, x.toFloat, y.toFloat)
	}
	
	def dispose(){
		img.dispose()
	}
	
	Image._cacheImage(this)
}

object Image{
	private var cache: Array[Image] = new Array[Image](0)
	
	def _cacheImage(img: Image): Unit = {
		cache = cache :+ img
	}
	
	def getImage(name: Symbol): Image = {
		var found_image: Image = null.asInstanceOf[Image]
		
		for(img <- cache){
			if(img.name == name){
				found_image = img
			}
		}
		found_image
	}
	
	def loadImages(images: Tuple2[Symbol, String]*){
		for(img <- images){
			new Image(img._1, img._2)
		}
	}
	
	def disposeAll(){
		for(img <- cache){
			img.dispose()
		}
	}
	
}
