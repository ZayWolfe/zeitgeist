package iris.axis.axr

import iris.axis.ax.Game
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion

class Sprite(var name: Symbol, img: Image, xcount: Int, ycount: Int, var fps: Int = Game.fps) {
	def this(name: Symbol, xcount: Int, ycount: Int){
		this(name, Image.getImage(name), xcount, ycount)
	}
	def this(name: Symbol, xcount: Int, ycount: Int, fps: Int){
		this(name, Image.getImage(name), xcount, ycount, fps)
	}
	
	private var cache: Array[TextureRegion] = new Array[TextureRegion](xcount * ycount)
	val width = img.width / xcount
	val height = img.height / ycount
	val frame = xcount * ycount
	var timestate = 0f
	
	//split the image up
	var tmp_cache: Array[Array[TextureRegion]] = TextureRegion.split(img._get_image_data, width, height)
	var index = 0
	for(i <- 0 until ycount){
		for(j <- 0 until xcount){
			cache(index) = tmp_cache(i)(j)
			index += 1
		}
	}
	tmp_cache = null.asInstanceOf[Array[Array[TextureRegion]]] //clear tmp
	
	def _draw_batch(btch: SpriteBatch, frame: Int = 0, x: Double = 0.0, y: Double = 0.0, rotation: Double = 0.0): Int = {
		var draw_frame = 0
		if(frame < cache.length){
			draw_frame = frame
		}
		btch.draw(cache(draw_frame), x.toFloat, y.toFloat, (width/2).toFloat, (height/2).toFloat, width.toFloat, height.toFloat, 1f, 1f, rotation.toFloat)
		
		draw_frame
	}
	
	Sprite._cacheSprite(this)
}

object Sprite{
	private var cache: Array[Sprite] = new Array[Sprite](0)
	
	def _cacheSprite(sprite: Sprite): Unit = {
		cache = cache :+ sprite
	}
	
	def getSprite(name: Symbol): Sprite = {
		var found_sprite: Sprite = null.asInstanceOf[Sprite]
		
		for(sprite <- cache){
			if(sprite.name == name){
				found_sprite = sprite
			}
		}
		found_sprite
	}
	def printSprites(){		
		println(cache.length)
		for(sprite <- cache){
			println(sprite.name)
		}
	}
	
	def loadSpritesAndImages(list: Tuple4[Symbol, String, Int, Int]*){
		for(img <- list){
			new Image(img._1, img._2)
		}
		for(sprite <- list){
			new Sprite(sprite._1, sprite._3, sprite._4)
		}
	}
	def loadSpritesAndImagesWFPS(list: Tuple5[Symbol, String, Int, Int, Int]*){
		for(img <- list){
			new Image(img._1, img._2)
		}
		for(sprite <- list){
			new Sprite(sprite._1, sprite._3, sprite._4, sprite._5)
		}
	}
	def loadSprites(sprites: Tuple3[Symbol, Int, Int]*){
		for(sprite <- sprites){
			new Sprite(sprite._1, sprite._2, sprite._3)
		}
	}
	def loadSpritesWFPS(sprites: Tuple4[Symbol, Int, Int, Int]*){
		for(sprite <- sprites){
			new Sprite(sprite._1, sprite._2, sprite._3, sprite._4)
		}
	}
	
}
