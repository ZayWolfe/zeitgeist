package iris.axis.ax.entity

import iris.axis.axr.Sprite
import iris.axis.ax._
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d
import com.badlogic.gdx.graphics.g2d.SpriteBatch

class Actor(val name: Symbol, val scene: Scene, var layer: Int = 0 ) {
	//the scene the actor is in 
	var cast: Symbol = 'none
	
	//physics and movement specific variables
    private var physics_enabled = false
    private var physics_body: box2d.Body = null
	var pos: Vector2 = new Vector2(0f, 0f)
	var vel: Vector2 = new Vector2(0f, 0f)
	var rotation = 0f
	
	//animation specific variables
	var animationSpeed = 1f	
	private var sprite: Sprite = null
	private var frame = 0
	private var frametime = 0f
	private var animation_enabled = false
	
	def _update(){
		if(physics_enabled){
	    	//update the physics body with user changes
	        physics_body.setTransform(pos, rotation * (math.Pi.toFloat/180))
			physics_body.applyForceToCenter(vel)
			
			//sets the position from the physics body
			pos.set(physics_body.getPosition())
			//conver to degrees
			rotation = physics_body.getAngle() * (180/math.Pi.toFloat)
		} else {
			pos.add(vel)
		}
	}
	
	def _draw(batch: SpriteBatch){
		if(animation_enabled){
			frametime += Gdx.graphics.getDeltaTime()/(1.toFloat/sprite.fps.toFloat)
			if(frametime > 1){
				frame += 1
				frametime = frametime%1f 
			}
			frame = sprite._draw_batch(batch, frame, 
				pos.x - sprite.width/2, 
				pos.y - sprite.height/2, rotation)
		}
	}
	
	def setAnimation(spriteName: Symbol, setFrame: Int = 0){
		var foundSprite = Sprite.getSprite(spriteName)
		
		if(foundSprite != null){
			animation_enabled = true
			sprite = foundSprite
			frame = setFrame
		}
		this
	}
	def removeAnimation(){
		animation_enabled = false
		sprite = null
		frame = 0
		this
	}
	
	//need to abstract this away more
	def setPhysics(
			shape: shapes.Shape, 
			_type: Symbol = 'dynamic, 
			density: Double = 1.0, 
			friction: Double = 0.2, 
			restitution: Double = 0.0
										): Actor = {
		//check if the old one exists, if so destroy it
		if(physics_body != null){
			scene.physics_world.destroyBody(physics_body)
		}
		
		var bodyDef = new box2d.BodyDef()
		bodyDef.position.set(pos.x, pos.y)
		
		//set the physics type for the object
		if(_type == 'dynamic){
			bodyDef.`type` = box2d.BodyDef.BodyType.DynamicBody
		} else if (_type == 'static){
			bodyDef.`type` = box2d.BodyDef.BodyType.StaticBody
		} else if (_type == 'kinematic){
			bodyDef.`type` = box2d.BodyDef.BodyType.KinematicBody
		}
		
		physics_body = scene.physics_world.createBody(bodyDef)
		var fixtureDef = new box2d.FixtureDef(); 
		fixtureDef.shape = shape.box2d_shape;  
		fixtureDef.density = density.toFloat;  
		fixtureDef.friction = friction.toFloat;  
		fixtureDef.restitution = restitution.toFloat;  
		physics_body.createFixture(fixtureDef);
		
		shape.dispose()
		this
	}
	
}

object Actor{
	private var cache: Array[Actor] = new Array[Actor](0)
	
	def _cacheActor(act: Actor): Unit = {
		cache = cache :+ act
	}
	
	def getActor(name: Symbol): Actor = {
		var found_Actor: Actor = null.asInstanceOf[Actor]
		
		for(act <- cache){
			if(act.name == name){
				found_Actor = act
			}
		}
		found_Actor
	}
	
	def disposeAll(){
		for(act <- cache){
			//act.dispose()
		}
	}
	
}
