package iris.axis.ax

import com.badlogic.gdx.ApplicationListener

/**Applet
 * Just a basic app object. All work is actually done inside
 * the Game class.
 */
class Applet(game: Game) extends ApplicationListener {
	def create(){game.create()}
	
	def render(){game.render()}
	
	def resize(width: Int, height: Int){game.resize(width,height)}
	
	def pause(){game.pause()}
	
	def resume(){game.resume()}
	
	def dispose(){game.dispose()}
}