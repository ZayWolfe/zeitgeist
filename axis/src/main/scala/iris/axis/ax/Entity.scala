package iris.axis.ax

class Entity( val name: Symbol, val initFunction: (scene: Scene, x: Float, y: Float)->Unit){
    
    def init(scene: Scene, x: Float = 0f, y: Float = 0f){ initFunction(scene, x, y)}
}

object Entity {
	private var cache = new Array[Entity](0)
	
	def _cacheEntity(entity: Entity): Unit = {
	    cache = cache :+ entity
	}
	
	def initEntity(name: Symbol, scene: Scene, x: Float = 0f, y: float = 0f): Unit = {
	    for(entity <- cache){
	        if(entity.name == name){
	            entity.init(scene, x, y)
	        }
	    }
	}
	    
	    
}
