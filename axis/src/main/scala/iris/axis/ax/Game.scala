package iris.axis.ax

import com.badlogic.gdx
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer

/** Game class
 * 
 * All actual work is done here and games
 * should extend this with their own main game classes.
 */
abstract class Game(
	val title: String = "Window", 
	val fps: Int = 30,
	val screenWidth: Int = 850, 
	val screenHeight: Int = 458,
	widthc: Int = 0, 
	heightc: Int = 0
	) {
	
	//if the coordinate dims are not given
	//default to screen dimensions.
	val width = if(widthc == 0) screenWidth else widthc
	val height = if(heightc == 0) screenHeight else heightc
	
	var app: Applet = _
	var debug2d_renderer: Box2DDebugRenderer = _
	
	def init(): Applet = {
		app = new Applet(this)
		Game.app = app
		Game.app
	}
	Game.fps = fps
	Game.width = width
	Game.height = height
	
	def create(){
		debug2d_renderer = new Box2DDebugRenderer()
		Gdx.input.setInputProcessor(new InputListener())
		start()
	}
	
	def render(){
		Gdx.gl.glClearColor(Game.background(0), Game.background(1), Game.background(2), Game.background(3))
		Gdx.gl.glClear(gdx.graphics.GL10.GL_COLOR_BUFFER_BIT)
		
		if(Game._current_scene != null){
			Game._current_scene._update()
			Game._current_scene._draw()
			draw()
			if(Game.debug)
				debug2d_renderer.render(Game._current_scene.physics_world, Game._current_scene.camera.combined)
		} else{
			draw()
		}
	}
	
	//methods to be overridden
	def start(){}
	
	def draw(){}
	
	def resize(width: Int, height: Int){}
	
	def pause(){}
	
	def resume(){}
	
	def dispose(){}
	
}
object Game {
	var _current_scene: Scene = null
	var app: Applet = null
	var fps = 30
	var width = 850
	var height = 458
	var background = Array(0.5f,0.5f,0.5f,1f)
	var debug = false
}

