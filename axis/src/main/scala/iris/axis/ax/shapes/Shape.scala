package iris.axis.ax.shapes

import com.badlogic.gdx.physics.box2d
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.physics.box2d.CircleShape
import com.badlogic.gdx.math.Vector2

abstract class Shape {
	//val _type: Symbol
	var box2d_shape: box2d.Shape
	val pos: Vector2
	var height: Float
	var width: Float
	
	
	def dispose(): Unit
}

class Box(widthc: Double = 50.0, heightc: Double = 50.0, posraw: (Double, Double) = (0.0,0.0)) extends Shape {
	var box = new PolygonShape();
		
	val pos = new Vector2(posraw._1.toFloat, posraw._2.toFloat)
	var width = widthc.toFloat
	var height = heightc.toFloat
	
	box.setAsBox(width, height, pos, 0.0f)
	
	var box2d_shape: box2d.Shape = box
	
	def dispose(){
		box2d_shape.dispose()
	}
}

class Circle(radius: Double = 6.0, posraw: (Double, Double) = (0.0,0.0)) extends Shape {
	var circle = new CircleShape()
	circle.setRadius(radius.toFloat)
	
	val pos = new Vector2(posraw._1.toFloat, posraw._2.toFloat)
	var height = radius.toFloat * 2f
	var width = height
	
	var box2d_shape: box2d.Shape = circle
	
	def dispose(){
		box2d_shape.dispose()
	}
}

class Polygon(vects: Array[Vector2], posraw: (Double, Double) = (0.0,0.0)) extends Shape{
	var poly = new PolygonShape()
	poly.set(vects)
	
	val pos = new Vector2(posraw._1.toFloat, posraw._2.toFloat)
	var box2d_shape: box2d.Shape = poly
	
	//getting the height
	var max = vects(0).y
	var min = max
	for(vect <- vects){
		max = max.max(vect.y)
		min = min.min(vect.y)
	}
	var height = max - min
	
	//getting the width
	max = vects(0).x
	min = max
	for(vect <- vects){
		max = max.max(vect.x)
		min = min.min(vect.x)
	}
	var width = max - min
	
	
	def dispose(){
		box2d_shape.dispose()
	}
}


object Shape {
	def projectArray(ratiox: Float, ratioy: Float, shapes: Array[Shape]){
		for(shape <- shapes){
			shape.pos.x = shape.pos.x * ratiox
			shape.pos.y = shape.pos.y * ratioy
			shape.width = shape.width * ratiox
			shape.height = shape.width * ratiox
		}
		Unit
	}
}

