package iris.axis.ax

import iris.axis.axr.Map
import iris.axis.ax.shapes.Shape
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.physics.box2d
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.Camera


class Scene(gravity_x: Double = 0.0, gravity_y: Double = -10.0, sleep: Boolean = true) {
	private var children: Array[Array[Actor]] = new Array[Array[Actor]](0)
	private var controllers = new Array[Controller](0)
	private var batch = new SpriteBatch()
	private var tiledMap: Map = _
	
	//make physics world
	var physics_world = new World(new Vector2(gravity_x.toFloat, gravity_y.toFloat), sleep)
	var border_shapes: Array[shapes.Shape] = new Array[shapes.Shape](0)
	//make a camera
	var camera = new OrthographicCamera()
	camera.setToOrtho(false, Game.width, Game.height)
	//renderer for the game maps
	
	def _update(){
		for(cont <- controllers){
			cont._update()
		}
		camera.update()
		for(layer <- children){
			for(actor <- layer){
				actor._update()
			}
		}
		physics_world.step(1/Game.fps.toFloat, 6, 2);
	}
	
	def _draw(){
		if(tiledMap != null){
			var map_key = 0
			var actor_key = 0
			var draw_actors = true
			var draw_map = true
			batch.setProjectionMatrix(camera.combined)
			while(draw_map || draw_actors){
				if(draw_map){
					tiledMap.renderer.render(camera, Array(map_key))
				}
				
				//render actors
				if(draw_actors){
					batch.begin()
					for(actor <- children(actor_key)){
						actor._draw(batch)
					}
					batch.end()
				}
				
				//increment which layer of the map to draw
				if(map_key < tiledMap.layer_size-1){
					map_key += 1
				} else {
					draw_map = false
				}
				
				//increment which actor layer to draw
				if(actor_key < children.length-1){
					actor_key += 1
				} else {
					draw_actors = false
				}
			}		
			
		} else {
			batch.setProjectionMatrix(camera.combined)
			batch.begin()
			for(layer <- children){
				//render actors
				for(actor <- layer){
					actor._draw(batch)
				}
			}
			batch.end()
		}
		
	}
	
	def _addActor(actor: Actor){
		//check if the layer exists
		if(actor.layer > children.length-1){
			children = children ++ Array.ofDim[Actor](
						actor.layer - (children.length-1), 0)
		}
		var layer = children(actor.layer)
		children(actor.layer) = layer :+ actor
	}
	
	
	def addMap(map: Map): Scene = {
		
		tiledMap = map
		
		border_shapes = map.border_shapes
		
		//lets find the scale factor
		
		var oldpos = new Vector3(border_shapes(0).pos.x,border_shapes(0).pos.y, 0f)
		var newpos = new Vector3(oldpos)
		camera.unproject(newpos)
		
		var ratiox = newpos.x / oldpos.x
		var ratioy = newpos.y / oldpos.y
		println(ratiox + ":" + ratioy)
		println(newpos.x + ":" + newpos.y)
		println(oldpos.x * ratiox + ":" + oldpos.y * ratioy)
		//Shape.projectArray(ratiox, ratioy, border_shapes)

		Scene.setBorderPhysics(physics_world, map.border_shapes)
		
		this
	}
	def addMap(name: String): Scene = {
		addMap(Map.getMap(name))
	}
	
	def _addController(cont: Controller){
		controllers = controllers :+ cont
	}
	val events = new EventHolder()
	
	
	Game._current_scene = this
}

object Scene {
	def setBorderPhysics(world: World, shapes: Array[Shape]) = {
		
		for(shape <- shapes){
			//println(shape.pos)
			var bodyDef = new box2d.BodyDef()
			bodyDef.position.set(shape.pos.x, shape.pos.y-shape.height)
			bodyDef.`type` = box2d.BodyDef.BodyType.StaticBody
			var body = world.createBody(bodyDef)
			var fixtureDef = new box2d.FixtureDef(); 
			fixtureDef.shape = shape.box2d_shape;  
			fixtureDef.density = 1f
			fixtureDef.friction = 0.2f
			fixtureDef.restitution = 0f  
			body.createFixture(fixtureDef);
			shape.dispose()
		}
		
	}
}
