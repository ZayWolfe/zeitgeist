package iris.axis.ax

import com.badlogic.gdx

class EventHolder {
	
	var events = new Array[(Symbol, Data=>Boolean, Boolean)](0)
	
	def add(_type: Symbol, func: Data=>Boolean): Unit = {
		events = events :+ (_type, func, true)
	}
	def fire(_type: Symbol, data: Data = new Data()): Unit = {
		for(event <- events){
			if(_type == event._1){
				event._2(data) //eventually allow removing with return false
			}
		}
		Unit
	}
}

class Controller(scene: Scene = Game._current_scene) {	
	class Control(var symbol: Symbol, val _type: Symbol = 'both, val func: ()=>Unit){}
	class Input(var symbol: Symbol, var code: Int = 0, var down: Boolean = false, var up: Boolean = false){}
	
	private var inputs = new Array[Input](0)
	private var controls = new Array[Control](0)
	
	//set up event listeners
	scene.events.add('keydown, (data:Data) => {
		var found = false
		for(input <- inputs){
			if(input.code == data.keyCode){
				found = true
				input.down = true
			}
		}
		//if the input wasn't found, add it
		if(!found){
			inputs = inputs :+ new Input(Symbol(data.keyCode.toString), data.keyCode, true)
		}
			
		true
	})
	scene.events.add('keyup, (data:Data) => {
		var found = false
		for(input <- inputs){
			if(input.code == data.keyCode){
				found = true
				input.down = false
				input.up = true
			}
		}
		//if the input wasn't found, add it
		if(!found){
			inputs = inputs :+ new Input(Symbol(data.keyCode.toString), data.keyCode, false, true)
		}
			
		true
	})
	
	
	def _update(){
		for(control <- controls){
			control._type match {
				case 'down => if(checkInputDown(control.symbol))control.func()
				
				case 'up => if(checkInputUp(control.symbol))control.func()
				
				case 'both => if(checkInputDown(control.symbol) || checkInputUp(control.symbol)){
					control.func()
				}
			}
		}
		//clean whether a input is 'up'
		for(input <- inputs){
			input.up = false
		}
	}
	
	def addControl(sym: Symbol, _type: Symbol, func: ()=>Unit): Controller = {
		controls = controls :+ new Control(sym, _type, func)
		this
	}
	
	def checkInputDown(sym: Symbol): Boolean = {
		var is_down = false
		
		for(input <- inputs){
			if(sym == input.symbol && input.down){
				is_down = true
			}
		}
		
		is_down
	}
	def checkInputUp(sym: Symbol): Boolean = {
		var is_up = false
		
		for(input <- inputs){
			if(sym == input.symbol && input.up){
				is_up = true
			}
		}
		
		is_up
	}
	
	scene._addController(this)
}

class InputListener extends gdx.InputProcessor {
	
	override def keyDown(keyCode: Int): Boolean = {
		val data = new Data()
		data.keyCode = keyCode
		Game._current_scene.events.fire('keydown, data)
		false
	}
	
	override def keyUp(keyCode: Int): Boolean = {
		val data = new Data()
		data.keyCode = keyCode
		Game._current_scene.events.fire('keyup, data)
		false
	}
	
	override def keyTyped( letter: Char): Boolean = {
		false
	}
	
	override def touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
		false
	}
	
	override def touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
		false
	}
	
	override def touchDragged(x: Int, y: Int, pointer: Int): Boolean = {
		false
	}
	
	override def mouseMoved(x: Int, y: Int): Boolean = {
		false
	}
	
	override def scrolled(ammount: Int): Boolean = {
		false
	}
	
	
	
}
