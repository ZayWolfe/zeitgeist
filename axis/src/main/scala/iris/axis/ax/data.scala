package iris.axis.ax

class Data{
	//actor update constructor
	def this(owner: Actor, exec_time: Double){
		this()
		this.owner = owner
		this.exec_time = exec_time
	}
	
	var keyCode: Int = 0
	var owner: Actor = null
	var exec_time: Double = -999.0
}
