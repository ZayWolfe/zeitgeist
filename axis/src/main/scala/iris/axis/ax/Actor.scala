package iris.axis.ax

import iris.axis.axr.Sprite
import com.badlogic.gdx.physics.box2d
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.Gdx

class Actor(x: Double = 0.0, y: Double = 0.0, layer: Int = 0, var name: String = "none", var _type: String = "none") {
	private val scene =  Game._current_scene
	private var frame = 0
	private var frametime = 0f
	private var draw_pos = new Vector2(x.toFloat, y.toFloat)
	var sprite: Sprite = _
	var animation_speed = 1f
	
	
	//set the physics object
	private var physics_body: Body = 
  _
	setPhysics(new shapes.Box(10,10))
	
	
	
	def _update(){
		if(physics_body != null){
			draw_pos.set(physics_body.getPosition())
		}
		
	}
	
	def _draw_batch(btch: SpriteBatch){
		if(sprite != null){
			frametime += Gdx.graphics.getDeltaTime()/(1.toFloat/sprite.fps.toFloat)
			if(frametime > 1){
				frame += 1
				frametime = frametime%1f 
			}
			frame = sprite._draw_batch(btch, frame, draw_pos.x - sprite.width/2, draw_pos.y -sprite.height/2)
		}
	}
	//user methods
	def setPosition(x: Double = draw_pos.x, y: Double = draw_pos.y){
		physics_body.setTransform(x.toFloat, y.toFloat, physics_body.getAngle())
		draw_pos.set(x.toFloat, y.toFloat)
	}
	
	def getLayer(): Int = {
		layer
	}
	
	def setSprite(spr: Sprite): Actor = {
		sprite = spr
		Actor.this
	}
	def setSprite(spr: String): Actor = {
		sprite = Sprite.getSprite(spr)
		Actor.this
	}
	
	def setPhysics(
			shape: shapes.Shape, 
			_type: Symbol = 'dynamic, 
			density: Double = 1.0, 
			friction: Double = 0.2, 
			restitution: Double = 0.0
										): Actor = {
		//check if the old one exists, if so destroy it
		if(physics_body != null){
			scene.physics_world.destroyBody(physics_body)
		}
		
		var bodyDef = new box2d.BodyDef()
		bodyDef.position.set(draw_pos.x, draw_pos.y)
		
		//set the physics type for the object
		if(_type == 'dynamic){
			bodyDef.`type` = box2d.BodyDef.BodyType.DynamicBody
		} else if (_type == 'static){
			bodyDef.`type` = box2d.BodyDef.BodyType.StaticBody
		} else if (_type == 'kinematic){
			bodyDef.`type` = box2d.BodyDef.BodyType.KinematicBody
		}
		
		physics_body = scene.physics_world.createBody(bodyDef)
		var fixtureDef = new box2d.FixtureDef(); 
		fixtureDef.shape = shape.box2d_shape;  
		fixtureDef.density = density.toFloat;  
		fixtureDef.friction = friction.toFloat;  
		fixtureDef.restitution = restitution.toFloat;  
		physics_body.createFixture(fixtureDef);
		
		shape.dispose()
		Actor.this
	}
	
	scene._addActor(Actor.this)
}