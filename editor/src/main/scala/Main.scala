package iris.zeit.editor

import iris.axis.ax._
import iris.axis.axr._
import iris.axis.axr.audio._
import iris.axis.axu._
import com.badlogic.gdx.backends.lwjgl.LwjglApplication

class Main extends Game("Axis Editor", 30, 1024, 600) {
	Game.debug = true
	
	override def start(){
		Sprite.loadSpritesAndImages(("new_button_main", "new_button_main.png", 1, 1))
		var start_scene = new Scene(0,0)
		
		new Actor(screenWidth/2, screenHeight/2).setSprite("new_button_main")
	}
	
	override def draw(){
		
	}
}

object Main {
	def main(args: Array[String]){
		var game: Main = new Main()
		println(game.screenWidth)
		
		new LwjglApplication(game.init(), game.title, game.screenWidth, game.screenHeight, true)
	}
}
