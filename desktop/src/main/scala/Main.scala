package iris.zeit.desktop

import iris.zeit.core.Zeitgeist
import com.badlogic.gdx.backends.lwjgl.LwjglApplication

object Main {
	def main(args: Array[String]){
		var game: Zeitgeist = new Zeitgeist("Zeitgeist",850, 600, 850, 600)
		
		new LwjglApplication(game.init(), game.title, game.screenWidth, game.screenHeight, true)
	}
}